# kylin-app-manager介绍
## 简介
+ 在传统Linux操作系统中，应用进程的生命周期一般由应用本身直接控制。然而，系统资源（包括CPU、I/O、存储等）是有限的，当我们运行大量的I/O密集型或CPU密集型应用时，经常会出现系统卡顿甚至死机的现象，这大大影响了用户的操作体验。所以，为了优化用户操作体验，在openKylin操作系统设计中，应用进程的生命周期并不由应用本身直接控制，而是由系统综合多种因素来确定，kylin-app-manager会对处于不同状态的应用进行分级处理，进行不同策略的CPU、磁盘I/O、内存等资源限制，尽可能地保障当前用户能够感知应用的资源分配，充分保证系统的整体流畅性，这是应用管理实现分级冻结的核心思想。
+ 应用管理还负责桌面应用程序启动，在平板模式下实现了应用单实例功能。

## 编译依赖
- `qtbase5-dev`
- `libkf5config-dev`
- `libkf5windowsystem-dev`
- `libkf5wayland-dev`
- `libkysdk-waylandhelper-dev`
- `libukui-log4qt-dev`
- ` pkg-config`
- `libgsettings-qt-dev`
- `libglib2.0-dev`

## 编译
```shell
mkdir build
cd build
qmake ..
make
```
## 使用要求
+ 当前系统需要支持cgroup v2版本，终端输入$ `mount | grep cgroup`，输出结果显示`cgroup2 on /sys/fs/cgroup`，说明支持cgroup v2系统。再输入$ `ll /sys/fs.cgroup`，输出结果有blick/、memory/和freezer/等目录结构的是cgroup v1结构，需要修改默认配置，$`sudo vim /etc/default/grub`,添加以下配置项 `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash systemd.unified_cgroup_hierarchy=1"`，然后输入$`sudo update-grub`， $`reboot`。
+ 重启后检验是否默认开启cgroup v2版本，终端输入$`ll /sys/fs/cgroup`，输出结果中显示`cgroup.controllers`和`cgroup.subtree_control`等系统文件的表明已开启cgroup v2结构。
+ 需要结合kylin-app-cgroupd共同实现分级冻结功能
+ kylin-app-cgroupd源码包地址：https://gitee.com/openkylin/kylin-app-cgroupd
## 应用状态的定义
+ 焦点应用：表示当前正在操作的即获取焦点的应用，不对其进行资源限制
+ 前台应用：只存在pc模式下的状态，表示当前运行的未最小化但没有获得焦点的应用
+ 后台应用：pc模式下指窗口最小化状态且持续时间<=30min或者前台应用持续时间>=30min的应用，平板模式下指非当前打开且持续时间<=30min的应用进程，平板模式下处于该状态下的多媒体进程需要暂停音视频的播放（dbus无法获取所有实例的状态
+ 缓存应用：指处于后台应用状态且持续时间>30min的应用进程，平板模式下，处于该状态的应用会被冻结
+ 休眠应用：被冻结并且放至swap分区的应用，在内存资源不足的情况下会转换至此状态

## 提供的Dbus功能接口 
+ kylin-app-manager 是一个开机自启动的dbus服务
- `type:         session dbus`
- `service:    com.kylin.AppManager`
- `path:         /com/kylin/AppManager`
-  `interface:  com.kylin.AppManager`

- `LaunchApp(in 's' desktopFile, out 'b' succeed) `
	+ 功能：打开desktopFile文件对应的应用程序

- `LaunchAppWithArguments(in 's' desktopFile, in 'as' args, out 'b' succeed)`
	+ 功能：打开desktopFile文件对应的应用程序。以启动参数args的方式运行

- `LaunchDefaultAppWithUrl(in 's' url, out 'b' succeed)`
	+ 功能：以默认应用打开url对应的资源

- `LaunchXdgFile(in 's' desktopFile, in 's' file, out 'b' succeed)`
	+ 功能：使用desktopFile对应的应用程序，打开file文件。

- `AppDesktopFileNameByWid(in 'qint' wid, out 's' desktopFile) `
	+ 功能：根据应用窗口wid的值找到应用对应的desktop名称。

- `ActiveProcessByWid(in 'uint' wid, out 'b' succeed) `
	+ 功能：此接口为应用打开时，通过多任务视图滑动关闭时提供，根据应用的窗口wid的值找到应用对应的进pid将其进程移动至ForegroundProcess分组，应用正常关闭。

- `AppWhiteList( in 's' option, out 'as' whiteLists)`
	+ 功能：获取某一个白名单名称下的所有应用列表

- `Open(int 's' fileName, out 'b' succeed)`
	+ 功能：打开文件

- `RecommendAppLists(in 's' fileName, out 'as' appLists)`
	+ 功能：获取某一个文件可支持打开的应用列表

- `AddToWhiteList(in 's' desktopFile, in 's' option, out 'b' succeed)`
	+ 功能：将desktopFile文件对应的应用程序添加到白名单。

- `WhiteListsOfApp(in 's' desktopFile, out 'as' whiteLists)`
	+ 功能：查询desktopFile文件对应的应用程序所在的白名单列表。

- `RemoveFromWhiteList(in 's' desktopFile, in 's' option, out 'b' succeed)`
	+ 功能：将desktopFile文件对应的应用程序从option对应的白名单中删除。

- `Inhibit(in 's' desktopFile, in 'u' pid,  in 's' reason, in 'i' flags, out 's' cookie)`
	+ 功能：通过该接口可设置指定运行应用不受某些资源使用的限制

- `UnInhibit(in 's' cookie)`
	+ 功能：取消Inhibit接口添加的限制

- `ThawApps(out 'b' succeed)`
	+ 功能：通过该接口解冻所有已打开应用

## 其他组件的交互
+ 所有需要启动第三方桌面应用或系统软件均需要通过应用管理的接口进行启动，比如开始菜单，侧边栏，任务栏，桌面和文件管理器等。

## 配置文件
+ com.kylin.AppManager.service 应用管理dbus自启动服务文件，保存在/usr/share/dbus-1/services目录下。
+ kylin-app-manager.json 应用管理资源限制策略保存在/etc/kylin-app-manager目录下。


## 联系我们
- `https://gitee.com/openkylin/kylin-app-manager`
