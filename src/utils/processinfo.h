/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESSINFO_H
#define PROCESSINFO_H

#include <QObject>
#include <KWindowInfo>

/**
 * @brief The ProcessInfo class 保存应用进程信息的类
 * @Info 保存的信息包含（pid childrenPid status wids args tasks exe cmdline）
 * @author duchangfeng
 */
class ProcessInfo : public QObject
{
    Q_OBJECT
public:
    enum AppCGroup {
        ForegroundProcess,
        InvisibleProcess,
        CachedProcess,
    };
    enum AppStatus {
        None,
        Launching,
        Running,
        Finished,
        Error,
    };
    enum AppType {
        Normal,
        Kmre,
    };

    explicit ProcessInfo(const ProcessInfo& proc);
    explicit ProcessInfo();

    bool operator==(const ProcessInfo &other);
    ProcessInfo& operator=(const ProcessInfo &other);

    qint64 pid() const;

    QList<qint64> childrenPids();

    AppCGroup cgroup() const;

    QList<uint> wids() const;

    QStringList getArgs() const;

    QList<qint64> getTasks();

    void setCGroup(const AppCGroup &appCGroup);

    void setArgs(const QStringList &appArgs);

    void setPid(const qint64 &appPid);

    void pushWid(const uint &wid);

    void removeWid(const uint &wid);

    void setWids(const QList<uint> &appWids);

    void setAppStatus(AppStatus status);
    void setLaunchPid(int pid);

    void setAppType(AppType type)
    { m_appType = type; }

    AppType appType() const
    { return m_appType; }

    void setDesktopFileName(const QString &desktopFileName)
    { m_desktopFileName = desktopFileName; }

    QString desktopFileName() const
    { return m_desktopFileName; }

    int launchPid() const;

    bool needingPolkitWindow() const
    { return m_needingPolkitWindow; }

    void setNeedingPolkitWindow(bool needing)
    { m_needingPolkitWindow = needing; }

    void setLockCgroup(bool locking);
    bool lockingCgroup() const;

    /**
     * @brief getCmdlineFromPid
     * @param pid 根据当前应用的pid获取Cmdline
     * @return
     */
    static QString getCmdlineFromPid(int pid);

    AppStatus appStatus() const;

    qint64 launchTimestamp() const
    { return m_launchTimestamp; }

    static QString cmdline(int pid);
    static qint64 parentId(int pid);

private:  
    void setChildrenPid(const qint64 &appPid);

    /**
     * @brief findChildrenPids 找到所有的子pid，包括孙pid
     * @param pid
     * @return
     */
    void findChildrenPids(qint64 pid);

    void setTasks(const qint64 &appPid);

    qint64 m_pid;
    QString m_procPath;
    QList<qint64> m_childrenPid;
    AppCGroup m_cgroup;
    QList<uint> m_wids;
    QStringList m_args;
    QList<qint64> m_tasks;
    QString m_exe;
    QString m_cmdline;

    QString m_directoryPath;
    QString m_filePath;

    AppStatus m_appStatus;
    AppType m_appType;
    QString m_desktopFileName;
    bool m_needingPolkitWindow;
    bool m_lockingCgroup;
    int m_launchPid;
    qint64 m_launchTimestamp;
};

QDebug &operator<<(QDebug &, const ProcessInfo &);

#endif // PROCESSINFO_H
