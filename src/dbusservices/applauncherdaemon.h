/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPLAUNCHERDAEMON_H
#define APPLAUNCHERDAEMON_H

#include <QObject>

class QSocketNotifier;
class QDBusInterface;

///
/// \brief The AppLauncherDaemon class
///
class AppLauncherDaemon : public QObject
{
    Q_OBJECT
public:
    explicit AppLauncherDaemon(QObject *parent = nullptr);

    // Unix signal handlers.
    static void chldSignalHandler(int unused);
    static void quitSignalHandler(int unused);

    static void setupUnixSignalHandlers();

public Q_SLOTS:
     // Qt signal handlers.
     void handleSigChld();
     void handleQuitSig();

private Q_SLOTS:
     void prepareForShutdown(bool shutDown);

Q_SIGNALS:
     void childProcessFinished(quint32 pid);
     void aboutToQuitAppManager();

private:
    static int s_sigChldFd[2];
    static int s_sigQuitFd[2];

    QSocketNotifier *m_snChld;
    QSocketNotifier *m_snQuit;

    QDBusInterface *m_sessionmanagerInterface;

    quint32 m_logoutInhibitCookie;
};

#endif // APPLAUNCHERDAEMON_H
