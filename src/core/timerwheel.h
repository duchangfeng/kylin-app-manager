/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIMERWHEEL_H
#define TIMERWHEEL_H

#include <functional>
#include "policy.h"

class QTimer;
namespace timerwheel {

struct AppData
{
    QString appId;
    QString instName;
    Policy::AppStatus dstStatus;
};

struct Timer
{
    quint64 timerId;
    int rotation;
    int tickSlot;
    std::function<void(AppData)> callback;
    AppData appData;
};

class TimerWheel : public QObject
{
public:
    explicit TimerWheel(QObject *parent = nullptr);
    quint64 addTimer(int timeout,
                     AppData appData,
                     const std::function<void(AppData)> &callback);

    bool deleteTimer(quint64 timerId);

private Q_SLOTS:
    void tick();

private:
    QTimer *m_timer;
    int m_currentSlot;
    quint64 m_currentTimerCount;
    QMap<int, QList<Timer *>> m_slotTimers;
    const int kSi = 1; // 1s
    const int kSlotNumber = 60;
};

} // namsapce timerwheol;


#endif // TIMERWHEEL_H
