/*
 * Copyright 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPCGROUP_H
#define APPCGROUP_H

#include <QObject>
#include "processmanager_interface.h"

class AppCGroup : public QObject
{
    Q_OBJECT
public:
    explicit AppCGroup(QObject *parent = nullptr);

    QString createProcessCGroup(const QString &appId, const QList<int> &pids);
    bool deleteProcessCGroup(const QString &cgroupName);
    bool setProcessCGroupResourceLimit(const QString &cgroupName,
                                       const QString &attrName,
                                       int value);
    bool reclaimProcesses(const QList<int> &pids);
    bool reclaimProcesses(const QString &cgroupName);
    
    QString cgroupNameWithPid(int pid);

private:
    void initCGroup();

private:
    com::kylin::ProcessManager *m_processManagerInterface;
    const char *kDbusResult = "result";
    const char *kDbusErrMsg = "errorMessage";
};

#endif // APPCGROUP_H
